function [C, sigma] = dataset3Params(X, y, Xval, yval)
%   [C, sigma] = EX6PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma.
%
C = 1;
sigma = 0.3;
C1 = [0.01;0.03;0.1;0.3;1;3;10;30];
sigma1 = [0.01;0.03;0.1;0.3;1;3;10;30];
err = zeros(8,8);
for i = 1:8
    for j = 1:8
        
        model = svmTrain(X,y,C1(i,1),@(x1,x2)gaussianKernel(x1,x2,sigma1(j,1)));
        pred = svmPredict(model, Xval);
        err(i,j) = mean(double(pred ~= yval));
    end
end
minMat = zeros(64,1);
errVec = err(:);
[minOfErrVec I] = min(errVec);
for i = 1:64
    if (i == I)
        minMat(i) = 1;
    end
end
minMatReshape = reshape(minMat(1:64),8,8);
for i =1:8
    for j = 1:8
        if (minMatReshape(i,j) == 1)
            C = C1(i);
            sigma = sigma1(j);
        end
    end
end
end
