% Initialization
clear; close all; clc


% Email Processing
fprintf('\nPreprocessing sample email...');
fid = fopen('emailSample1.txt');
file_contents = fscanf(fid, '%c', inf);
fclose(fid);
word_indices  = processEmail(file_contents);
% Print Stats
fprintf('Word Indices: \n');
fprintf(' %d', word_indices);
fprintf('\n\n');
fprintf('\nExtracting features from sample email (emailSample1.txt)\n');
% Extract Features
fid = fopen('emailSample1.txt');
file_contents = fscanf(fid, '%c', inf);
fclose(fid);
word_indices  = processEmail(file_contents);
features      = emailFeatures(word_indices);
% Print Stats
fprintf('Length of feature vector: %d\n', length(features));
fprintf('Number of non-zero entries: %d\n', sum(features > 0));


% Training SVM
load('spamTrain.mat');
fprintf('\nTraining Linear SVM (Spam Classification)\n')
C = 0.1;
model = svmTrain(X, y, C, @linearKernel);
p = svmPredict(model, X);
fprintf('Training Accuracy: %f\n', mean(double(p == y)) * 100);


%Testing
load('spamTest.mat');
fprintf('\nEvaluating the trained Linear SVM on a test set ...\n')
p = svmPredict(model, Xtest);
fprintf('Test Accuracy: %f\n', mean(double(p == ytest)) * 100);

